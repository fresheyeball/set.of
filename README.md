# Sets of n

This is a libary for Sets of a fixed size, using a similar appoach to the much beloved `Data.Set` module.
We use TypeLits to allow type safe operations.

[![pipeline status](https://gitlab.com/fresheyeball/set.of/badges/master/pipeline.svg)](https://gitlab.com/fresheyeball/set.of/commits/master)